
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Point p = new Point(3, 5);
		
		System.out.println("x = " + p.xCoord + " y = " + p.yCoord);
		
		
		Rectangle r = new Rectangle(6, 8, new Point (6,6));
		
		int area = r.area();
		System.out.println(area);
		
		Rectangle r2 = new Rectangle(4,7,new Point (3,2));
		
		System.out.println(r2.area());
		
		System.out.println(r2.perimeter());
		
		Point[] corners = r2.corners();
		for (int i = 0; i < corners.length; i++) {
			System.out.println("x = " + corners[i].xCoord + " y = " + corners[i].yCoord);
			
		}
		
		Circle c = new Circle(8,new Point(5,5));
		
		System.out.println(c.area());
		System.out.println(c.overlap(new Circle(8,new Point(5,5))));
	}

}
